# CONSTANTES ORDONNÉES [ordef] pour SPIP 3

## Pourquoi ce plugin ?
La logique de surchage de SPIP est plutôt efficace, avec cependant un bémol pour la gestion des `_CONSTANTES PHP` qui, par définition, échappe à la logique de surcharge.
Dans certains cas c'est un problème, notamment lorsque les constantes concernent des éléments de personnalisation que l'on aimerait ajuster au niveau d'un sous-plugin, alors qu'un plugin parent en a déjà fixer la valeur, car arrivé plus tôt dans la cascade de dépendances.
Généralement, on peut s'en tirer en éditant `config/mes_options.php` qui reste prioritaire sur tout le reste. GROSSE lacune cependant, on se retrouve alors à devoir doublonner le versionnage d'un projet : le plugin d'un côté ET le fichier mes_options de l'autre.

## Que fait ce plugin ?
Il se contente de lire le contenu d'un pipeline déclaré *pre_plugins_options* et de transposer les clé|valeurs éventuellement reçues (et qui elles sont surchargeables), en constantes php lors de l'écriture du fichier  `./tmp/cache/charger_plugins_options.php.` (visiter ./ecrire/?exec=admin_plugin&actualise=1 pour que les modifications soient prises en compte).

## Comment ça marche ?
C'est aux (sous-)plugins qui en ont besoin de déclarer l'usage du pipeline : 

```php
/**
 * Exemple d'usage du pipeline pre_plugins_options
 * 
 * On remplit un tableau associatif cle => valeur sur un niveau
 * La clé respecte la convention d'écriture des contantes: en _MAJUSCULES, généralement préfixée d'un tiret '_'
 * 
 * 
 * @param array $flux
 * @return array $flux
 */
function xxx_pre_plugins_options($flux) {

	// options de dev
	if (preg_match_all(",(^dev\.)|(\.loc$),m", $_SERVER['HTTP_HOST'] )) {
		$flux['_NO_CACHE'] = -1;
		$flux['_INTERDIRE_COMPRESSION_HTML'] = true;
	} 
	// options de prod
	else {
		$flux['_JS_ASYNC_LOAD'] = true;
	}
	// dans tous les cas
	$flux['_ALBUMS_RECADRER'] = true;
	$flux['_ALBUMS_TAILLE_PREVIEW'] = 200;
	$flux['_IMG_MAX_WIDTH'] = 2400;
	$flux['_IMG_MAX_HEIGHT'] = 1800;
	$flux['_AUTOBR'] = '';
	$flux['_IMAGE_RESPONSIVE_CALCULER'] = true;

	return $flux;
}
```
----
Le plugin embarque une surcharge du fichier `./ecrire/inc/plugin.php`
Voir sur core.spip.net : tickets [4102](https://core.spip.net/issues/4102) | [4105](https://core.spip.net/issues/4105).